package ar.unq.edu.epers.services

import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date

class ServicioReservaAutos {
	
	ServicioReservaHbm reservaHibernate
	ServicioReservaCassandra reservaCassandra
	ServicioAutoHbm autosHmb
	
	new (){
		reservaHibernate = new ServicioReservaHbm
		reservaCassandra = new ServicioReservaCassandra
		autosHmb=new ServicioAutoHbm
	}
	
	def guardar(Reserva reserva){
		reservaHibernate.guardar(reserva)
		this.actualizarCache(reserva)
	}
	
	def getAutosPara(Ubicacion ubicacion,Date fecha){
		var autos =reservaCassandra.recuperarAutosParaUbicacionYFecha(fecha,ubicacion).patentesDeAutos
		if(autos==null){
			autos=autosHmb.disponiblesEnUbicacionYDia(ubicacion,fecha).map[it.patente].toList
		    autos.forEach[reservaCassandra.guardarAuto(fecha,ubicacion,it)
		    ]	
		}
		
		autos
	}
	def actualizarCache(Reserva reserva){
		reservaCassandra.actualizarCacheAlRealizarReserva(reserva.inicio,reserva.fin,reserva.origen, reserva.destino,reserva.auto.patente)
	}
}