package ar.unq.edu.epers.services

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Calificacion
import ar.unq.edu.epers.model.Comentario
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.model.Visibilidad
import ar.unq.edu.epers.mongoDB.SistemDB
import org.mongojack.DBQuery

class ServicioComentarios {
	
	
	def void calificar(Auto auto, Usuario usuario, String texto, Calificacion calificacion, Visibilidad visi){
		val homeComentario = SistemDB.instance().collection(Comentario);
		val comentarioNuevo = new Comentario(auto.marca, auto.modelo, usuario.userName, texto, calificacion, visi)
		homeComentario.insert(comentarioNuevo)
	}
	
	def cantCalificaciones(){
		val homeComentario = SistemDB.instance().collection(Comentario)
		var coleccion = homeComentario.mongoCollection.find()
		return coleccion.size 
	}	
	
	def verPerfil(Usuario yo, Usuario aVer){
		val homeComentario = SistemDB.instance().collection(Comentario);
		var servicioAmigos= new ServicioAmigos
		if(yo.userName.equals(aVer.userName)){ 
			return homeComentario.mongoCollection.find(DBQuery.is("nombreUsuario", yo.userName) )	
	    }
		if(servicioAmigos.esAmigoDe(yo, aVer)){
			return homeComentario.mongoCollection.find(DBQuery.in("visibilidad", Visibilidad.PUBLICO,Visibilidad.SOLO_AMIGOS).and (DBQuery.is("nombreUsuario", aVer.userName)))
			
		}
		if (!servicioAmigos.esAmigoDe(yo, aVer)){
			return homeComentario.mongoCollection.find(DBQuery.is("visibilidad", Visibilidad.PUBLICO)).and (DBQuery.is("nombreUsuario", aVer.userName))
			
		}
		
	}
	
	def purgeDB(){
		val homeComentario = SistemDB.instance().collection(Comentario);
		homeComentario.mongoCollection.drop
	}
}