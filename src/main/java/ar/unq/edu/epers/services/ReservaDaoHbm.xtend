package ar.unq.edu.epers.services

import ar.unq.edu.epers.dao.hbm.GenericDao
import ar.unq.edu.epers.model.Reserva

class ReservaDaoHbm extends GenericDao<Reserva> {
	
	new(Class<Reserva> modelType) {
		super(modelType)
	}
	
	
}