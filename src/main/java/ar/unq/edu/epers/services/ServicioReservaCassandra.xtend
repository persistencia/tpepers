package ar.unq.edu.epers.services

import ar.unq.edu.epers.dao.CacheDao
import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date
import org.joda.time.DateTime

class ServicioReservaCassandra {

 	CacheDao dao
 
 	new(){
 		dao= new CacheDao
 	} 
 
 	
 	def guardarAuto (Date fecha, Ubicacion ubicacion , String patente){
		var autosEnUbicacionYFecha = dao.getPorUbicacionYFecha(ubicacion.nombre, fecha)
		var patentes = newArrayList
		patentes.add(patente)
		if (autosEnUbicacionYFecha == null) dao.saveReserva(ubicacion.nombre, fecha, patentes) 
		else {autosEnUbicacionYFecha.agregarAuto(patente)
		dao.saveReserva(ubicacion.nombre, fecha, autosEnUbicacionYFecha.patentesDeAutos)}
	}
	
	def recuperarAutosParaUbicacionYFecha(Date fecha, Ubicacion ubi){
		dao.getPorUbicacionYFecha(ubi.nombre, fecha)
	}
	
	def actualizarCacheAlRealizarReserva(Date fechaInicio,Date fechaFin, Ubicacion origen, Ubicacion destino, String patente){
		var fecha=new DateTime(fechaInicio)
		var fechaFinJoda=new DateTime(fechaFin).plusDays(1)
		
		while(fecha.isBefore(fechaFinJoda)){
		  var fechaReserva = fecha.toDate
		  delete(origen.nombre, fechaReserva, patente)
		  fecha = fecha.plusDays(1)
		}
		
	} 
	
	def cleanDB(){
		dao.clean
	}
	
	def delete(String ubicacion, Date fecha, String patente){
		dao.deleteReserva(ubicacion,fecha,patente)
	}
}