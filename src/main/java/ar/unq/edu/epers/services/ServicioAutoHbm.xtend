package ar.unq.edu.epers.services

import ar.unq.edu.epers.dao.hbm.AutoDaoHbm
import ar.unq.edu.epers.hbmUtils.SessionManager
import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date
import java.util.List

class ServicioAutoHbm extends ServicioAuto{


	new(){}
	
	override disponiblesEnUbicacionYDia(Ubicacion ubicacion, Date dia) {
		SessionManager.runInSession([|
			var disponibles = new AutoDaoHbm().getAll 
			disponibles.filter[ubicacionParaDia(dia).nombre.equals(ubicacion.nombre)].toList
			]);
	}
	
	
	override disponiblesEn(Ubicacion origen, Ubicacion destino, Date inicio, Date fin, Categoria categoria) {
		SessionManager.runInSession([|
		new AutoDaoHbm().getAutosConParametros(origen,destino,inicio,fin,categoria) as List<Auto>
				]);
	}
	
	
	def getAutoPorNroPatente(String patente){
		SessionManager.runInSession([|
		new AutoDaoHbm().getPorNroPatente(patente)
		]);
	}
	
	def guardarAuto(Auto a){
		SessionManager.runInSession([|
		new AutoDaoHbm().save(a)
		1
		]);
	}
	
	def eliminarAuto(Auto a){
		SessionManager.runInSession([|
		new AutoDaoHbm().delete(a)
		1
		]);
	}
	
}