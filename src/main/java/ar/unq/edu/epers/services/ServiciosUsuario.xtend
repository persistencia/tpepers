package ar.unq.edu.epers.services

import ar.unq.edu.epers.model.Usuario

/**
 * Esta interfaz define las operaciones a utilizar para manejar los usuarios en el sistema
 */
interface ServiciosUsuario {
	def void registrarUsuario(Usuario usuarioNuevo)throws UsuarioYaExisteException
	def void validarCuenta (String codigoValidacion)throws ValidacionException
	def Usuario loggearUsuario (String userName, String password)throws UsuarioNoExiste
	def Boolean cambiarPassword (String userName, String passwordVieja, String nuevapassword)throws NuevaPasswordInválida
}

/**
 * Envia un mensaje de error cuando no se cumplen los requisitos para la contraseña
 */
class NuevaPasswordInválida extends Exception{
	new(String message) {
		super(message)
	}
}

/**
 * Envia un mensaje de error cuando el usuario a logear no existe
 */
class UsuarioNoExiste extends Exception{
	
	new(String message) {
		super(message)
	}
}

/**
 * Envia un mensaje de error cuando no se puede validar la cuenta
 */
class ValidacionException extends Exception{
	
	new(String message) {
		super(message)
	}
	
}

/**
 * Envia un mensaje de error cuando el usuario a registrar ya existe
 */
class UsuarioYaExisteException extends Exception{
	new(String message) {
		super(message)
	}
}

