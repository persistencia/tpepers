package ar.unq.edu.epers.services

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Empresa
import ar.unq.edu.epers.model.IUsuario
import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.model.ReservaEmpresarial
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date
import java.util.List
import ar.unq.edu.epers.hbmUtils.SessionManager

class ServicioCompleto  {
	
	new(){

	}
	
	def hacerReserva(Integer numeroSolicitud,Ubicacion origen,Ubicacion destino,Date inicio,Date fin,Auto auto,IUsuario usuario) {

		var reserva= new Reserva =>[it.numeroSolicitud=numeroSolicitud 
			                        it.origen=origen 
			                        it.destino=destino 
			                        it.inicio=inicio 
			                        it.fin=fin 
			                        it.auto=auto 
			                        it.usuario=usuario
		]
		reserva.reservar
		new ServicioReservaHbm().guardar(reserva)
	

	}
	def hacerReservaEmpresrial(Integer numeroSolicitud,Ubicacion origen,Ubicacion destino,Date inicio,Date fin,Auto auto,IUsuario usuario,Empresa empresa,String nombreContacto,String cargoContacto){

		var reserva= new ReservaEmpresarial =>[it.numeroSolicitud=numeroSolicitud 
			                        it.origen=origen 
			                        it.destino=destino 
			                        it.inicio=inicio 
			                        it.fin=fin 
			                        it.auto=auto 
			                        it.usuario=usuario
			                        it.empresa = empresa
			                        it.nombreContacto = nombreContacto
			                        it.cargoContacto = cargoContacto
		]
		reserva.reservar
		new ServicioReservaHbm().guardar(reserva)
		
	}
	
	def List<Auto> autosDisponiblesEnUbicacionYDia(Ubicacion ubicacion, Date dia){
		SessionManager.runInSession([|
		new ServicioAutoHbm().disponiblesEnUbicacionYDia(ubicacion,dia) as List<Auto>
		]);
	}
	
	def List<Auto> autosDisponiblesEn(Ubicacion origen, Ubicacion destino, Date inicio, Date fin, Categoria categoria){
		SessionManager.runInSession([|
		new ServicioAutoHbm().disponiblesEn(origen,destino,inicio,fin,categoria)
		]);
	}
	
	
}