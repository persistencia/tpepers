package ar.unq.edu.epers.services

import ar.unq.edu.epers.dao.AmigosDao
import ar.unq.edu.epers.model.Mensaje
import ar.unq.edu.epers.model.TipoDeRelacion
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.neo4jServices.GraphServiceRunner
import java.util.HashSet
import java.util.Set
import org.neo4j.graphdb.GraphDatabaseService
import java.util.ArrayList
import java.util.List

class ServicioAmigos{
	
	private def createHome(GraphDatabaseService graph) {
		new AmigosDao(graph)
	}
	
	def void crearUsuario(Usuario u){
		GraphServiceRunner::run[
			createHome(it).crearNodo(u); 
			null
		]
	}
	
	def void eliminarUsuario(Usuario u){
		GraphServiceRunner::run[
			createHome(it).eliminarNodo(u)
			null
		]
	}
	
	def void amigoDe(Usuario from, Usuario to){
		GraphServiceRunner::run[
			val home = createHome(it);
			home.relacionarAmigo(from, to, TipoDeRelacion.AMIGO)
		]
	}
	
	def Set<Usuario> getAmigos(Usuario usr){
		GraphServiceRunner::run[
			val home = createHome(it);
			home.getAmigos(usr)
		]
	}
	
	
	def Set<Usuario> getAmigosSinMi(Usuario usr, Usuario yo){
		val amiguitos= getAmigos(usr).filter[it.userName != yo.userName]
		amiguitos.toSet
	}
	
	def void agregarAmigo(Set<Usuario> set, Usuario yo){
		if(set.forall[userName != yo.userName])
		set.add(yo)
	}
	
	def void agregarAmigos(Set<Usuario> set, Set<Usuario> setAmigos){
		for(Usuario u : setAmigos){
			agregarAmigo(set, u)
		}		
	}
	
	def void mandarMensaje(Usuario to,Usuario from ,String mensaje){
		GraphServiceRunner::run[	
			createHome(it).enviarMensaje( new Mensaje(to,from,mensaje) )
			null
		]
	}
	
	def Set<Usuario> getConectados(Usuario yo){
		GraphServiceRunner::run[
		var todos= new HashSet
		var misAmigos= getAmigos(yo)
		todos.addAll(misAmigos)
		for(Usuario u : misAmigos){
			todos.agregarAmigos(getAmigosSinMi(u, yo))//getAmigosSinMi 	
		}
		todos
		]
	}
	
	def getMensajesDestinatario(String userName){
		GraphServiceRunner::run[
			createHome(it).getMensajeDestinatario(userName)
		]
	}
	
	def getMensajesRemitente(String userName){
		GraphServiceRunner::run[
			createHome(it).getMensajesRemitente(userName)
		]
	}
	
	def Boolean esAmigoDe(Usuario user, Usuario aVer){
		GraphServiceRunner::run[
			createHome(it).esAmigoDe(user.userName,aVer.userName)
		]
	}
	
}