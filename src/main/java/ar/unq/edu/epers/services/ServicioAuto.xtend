package ar.unq.edu.epers.services

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date
import java.util.List

abstract class ServicioAuto {
	
	def Iterable<Auto> disponiblesEnUbicacionYDia(Ubicacion ubicacion, Date dia)
	
	def List<Auto> disponiblesEn(Ubicacion origen, Ubicacion destino, Date inicio, Date fin, Categoria categoria)
	
	
	
}