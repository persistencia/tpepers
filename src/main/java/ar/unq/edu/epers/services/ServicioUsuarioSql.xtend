package ar.unq.edu.epers.services

import ar.unq.edu.epers.dao.UsuarioDaoSql
import ar.unq.edu.epers.mailing.EnviadorDeMails
import ar.unq.edu.epers.mailing.Mail
import ar.unq.edu.epers.model.CodeGenerator
import ar.unq.edu.epers.model.Usuario

/**
 * Implementa los métodos para manejar los usuarios en el sistema
 */
class ServicioUsuarioSql implements ServiciosUsuario{
	
	UsuarioDaoSql usuarioDao
	
	EnviadorDeMails enviadorDeMails
	
	new(UsuarioDaoSql dao, EnviadorDeMails enviador){
		usuarioDao = dao
		enviadorDeMails = enviador
	}
	
	/**
	 * Registra a un usuario nuevo en el sistema comprobando que los datos son válidos y genera
	 * un nuevo código de validación que será asociado al usuario.
	 * Lanza las excepciones correspondientes de existir algún error
	 */
	override registrarUsuario(Usuario usuarioNuevo) {
		val usr = usuarioDao.getUsuarioPorUsername(usuarioNuevo.userName)
		if (usr == null){
			val codigo=CodeGenerator.newCode(usuarioNuevo)
			usuarioNuevo.codigoValidacion=codigo
			val m= new Mail =>[
				subject='Validation Mail'
				body='Debe validar su cuenta introduciendo el siguiente codigo:'+codigo
				to=usuarioNuevo.direccionMail
				from='sistema@prueba.com'
			]
			enviadorDeMails.enviarMail(m)
			usuarioDao.insertUsuario(usuarioNuevo)
		}
		else 
			throw new UsuarioYaExisteException("El usuario ya existe en el sistema")	
	}
	
	/**
	 * Valida un usuario comprobando que el código insertado es el mismo al código de validación 
	 * asociado a dicho usuario
	 * Lanza las excepciones correspondientes si existe algun error
	 */
	override validarCuenta(String codigoValidacion) {
		val usuario=usuarioDao.getUsuarioPorCodigoValidacion(codigoValidacion);
		if(usuario==null){
			throw new ValidacionException("No existe el usuario con ese codigo de validacion");
		}
		usuario.validate();
		usuarioDao.updateUsuario(usuario);
		
		
	}
	
	/**
	 * Permite al usuario entrar al sistema comprobando que sus datos y contraseña son correctos.
	 * Lanza las excepciones correspondientes si existe algun error
	 */
	override loggearUsuario(String userName, String password) {
		val usuario =usuarioDao.getUsuarioPorUsername(userName)
		if(usuario==null)
			throw new UsuarioNoExiste("No existe el usuario con ese userName")
		
		if(!usuario.validatePassword(password))
			throw new UsuarioNoExiste("La password no es valida para ese userName")
		
		
		usuario
		
	}
	
	/**
	 * Comprueba que la nueva contraseña sea válida (que no sea igual a la ya utilizada) y la almacena 
	 * en la base de datos.
	 * Lanza las excepciones correspondientes si existe algun error
	 */
	override cambiarPassword(String userName, String passwordVieja, String nuevaPassword) {
		val usuario = usuarioDao.getUsuarioPorUsername(userName)
		if (! passwordVieja.equals(nuevaPassword)) {
			usuario.changePassword(nuevaPassword)
			usuarioDao.updateUsuario(usuario)
			}
		else 
			throw new NuevaPasswordInválida ("La contraseña a ingresar es igual a la establecida")
		
		nuevaPassword.equals(usuario.password)
	}
	
	
	
}