package ar.unq.edu.epers.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.mongojack.ObjectId
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Comentario {
	
	String marca
	String modelo
	String nombreUsuario
	String texto
	Calificacion calificacion
	Visibilidad visibilidad
	@ObjectId
	@JsonProperty("_id")
	String id
	
	new (){}
	
	new (String marc, String model, String userName, String coment, Calificacion calif, Visibilidad visi) {
		this.marca= marc
		this.modelo= model
		this.nombreUsuario= userName
		this.texto = coment
		this.calificacion = calif
		this.visibilidad = visi
	}                       
}
