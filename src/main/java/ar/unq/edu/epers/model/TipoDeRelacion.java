package ar.unq.edu.epers.model;

import org.neo4j.graphdb.RelationshipType;

public enum TipoDeRelacion implements RelationshipType {
	AMIGO, REMITENTE, DESTINATARIO
}