package ar.unq.edu.epers.model

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Mensaje {
	Usuario to;
	Usuario from;
	String mensaje;
	
	new(Usuario to, Usuario from, String mensaje){
		this.to=to
		this.from=from
		this.mensaje=mensaje
	}
	
	new() {
	}
	
	
}