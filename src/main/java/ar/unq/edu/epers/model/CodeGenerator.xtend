package ar.unq.edu.epers.model

class CodeGenerator {
	
	/**
	 * Genera un nuevo código de validación; el mismo es un String formado a partir de la conjunción
	 * del nombre de usuario, la contraseña, la cantidad de caracteres del nombre y de la direccion
	 * de mail del usuario enviado por parámetro
	 */
	def static newCode(Usuario usuario) {
		val codigo = new StringBuilder()
		codigo => [
			append(usuario.userName)
			append(usuario.password)
			append(usuario.nombre.length.toString())
			append(usuario.direccionMail.length.toString())
		] 
		return codigo.toString()
	}
	
}