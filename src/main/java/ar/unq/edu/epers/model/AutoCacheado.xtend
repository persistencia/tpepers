package ar.unq.edu.epers.model

import com.datastax.driver.mapping.annotations.UDT
import org.eclipse.xtend.lib.annotations.Accessors
import com.datastax.driver.mapping.annotations.Field

@UDT (name ="autoCacheado")
@Accessors
class AutoCacheado {
	@Field(name = "marca")
	String marca
	@Field(name = "modelo")
	String modelo
	@Field(name = "patente")
	String patente
	
	new(String marca, String modelo, String patente){
		this.marca = marca
		this.modelo = modelo
		this.patente = patente
	}
	
	def equals(AutoCacheado auto){
		this.patente.equals(auto.patente)
	}
}