package ar.unq.edu.epers.model

import com.datastax.driver.mapping.annotations.Column
import com.datastax.driver.mapping.annotations.PartitionKey
import com.datastax.driver.mapping.annotations.Table
import java.util.Date
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
@Table(keyspace = "persistenciaReservas", name = "autosDisponibles")

class BusquedaDeReservasCache {
	@PartitionKey(0)
	@Column (name="ubicacion")
	String ubicacion
	@PartitionKey(1)
	@Column (name="dia")
	Date dia
	@Column (name="autosCacheados")
	List<String> patentesDeAutos 
	
	new(){}
	
	new(Date dia, String ubicacion, List<String> ats){
		this.ubicacion = ubicacion
		this.dia = dia
		this.patentesDeAutos = ats
	}
	
	def agregarAuto(String autitoPatente){
		patentesDeAutos.add(autitoPatente)
	}
	
	def borrarAuto(String autitoPatente){
		patentesDeAutos.remove(autitoPatente)
	}
	
	def tieneAuto(AutoCacheado autitoPatente){
		patentesDeAutos.contains(autitoPatente)
	}
}