package ar.unq.edu.epers.model

enum Calificacion {
	
	EXCELENTE, BUENO, REGULAR, MALO
}