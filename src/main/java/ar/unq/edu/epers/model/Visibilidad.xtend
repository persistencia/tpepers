package ar.unq.edu.epers.model

enum Visibilidad {
	PRIVADO, PUBLICO, SOLO_AMIGOS
}