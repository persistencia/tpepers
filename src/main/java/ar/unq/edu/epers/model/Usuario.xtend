package ar.unq.edu.epers.model

import java.util.Date
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * Esta clase representa al usuario con sus parametros necesarios
 */
@Accessors class Usuario implements IUsuario {
	
	var Integer idUsuario
	var String nombre
	var String apellido
	var String userName
	var String password
	var String direccionMail
	var Date fechaNacimiento
	var Boolean validated=false //Seteado en false hasta que se compruebe la validacion del usuario
	var String codigoValidacion
	List<Reserva> reservas
	
	new(){}
	
	/**
	 * Valida al usuario en el sistema
	 */
	def validate() {
		validated=true
	}
	
	/**
	 * Este método valida la contraseña del usuario
	 */
	def validatePassword(String password) {
		this.password==password
	}
	
	/**
	 * Este método cambia la contraseña del usuario
	 */
	def changePassword(String newPassword) {
		this.password = newPassword
	}
	
	override agregarReserva(Reserva unaReserva) {
		reservas.add(unaReserva)
	}
	
	def boolean equals(Usuario u){
		u.userName == this.userName
	}
	
	
}