package ar.unq.edu.epers.mongoDB;

import com.mongodb.DB
import com.mongodb.MongoClient
import java.net.UnknownHostException
import org.mongojack.JacksonDBCollection

class SistemDB {
	static SistemDB INSTANCE;
	MongoClient mongoClient;
	DB db;

	synchronized def static SistemDB instance() {
		if (INSTANCE == null) {
			INSTANCE = new SistemDB();
		}
		return INSTANCE;
	}

	private new() {
		try {
			mongoClient = new MongoClient("localhost", 27018);
		} catch (UnknownHostException e) {
			throw new RuntimeException(e); 
		}
		db = mongoClient.getDB("persistencia");
	}
	
	
	def <T> ar.unq.edu.epers.mongoDB.Collection<T> collection(Class<T> entityType){
		val dbCollection = db.getCollection(entityType.getSimpleName());
		new ar.unq.edu.epers.mongoDB.Collection<T>(JacksonDBCollection.wrap(dbCollection, entityType, String));
	}

}
