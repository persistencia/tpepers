package ar.unq.edu.epers.dao.hbm

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.model.Ubicacion
import java.util.Date
import java.util.List
import org.hibernate.Criteria
import org.hibernate.criterion.Restrictions
import ar.unq.edu.epers.hbmUtils.SessionManager

class AutoDaoHbm extends GenericDao<Auto>  {
	
	new() {
		super(Auto)
	}
	
	def getPorNroPatente(String patente) {
	  SessionManager.getSession().createCriteria(Auto).add(Restrictions.like("patente",patente)).list.get(0) as Auto
	  //sessionFactory.openSession.createCriteria(Auto).add(Restrictions.like("patente",patente)).list.get(0) as Auto
	}
	

	
	def List<Auto> getAutosConParametros(Ubicacion origen,Ubicacion destino,Date inicio,Date fin,Categoria categoria){
		/*var query=SessionManager.session.createQuery("SELECT AUTO.* FROM AUTO JOIN RESERVA" )
		query.*/
		
		var criteria= SessionManager.getSession().createCriteria(Reserva,"reservas")//.createAlias("re.auto","reau")
		  
		//var criteria=sessionFactory.openSession.createCriteria(Reserva,"reservas")
		
		  this.addRestrictionIfNotNull("origen",origen,criteria)
		  this.addRestrictionIfNotNull("destino",destino,criteria)
		  this.addRestrictionIfNotNull("inicio",inicio,criteria)
		  this.addRestrictionIfNotNull("fin",fin,criteria)
		  criteria=criteria.createCriteria("auto")
		  this.addRestrictionIfNotNull("categoria",categoria,criteria)
		  
		  criteria.list
	}
	
	def private void addRestrictionIfNotNull(String property, Object objeto,Criteria criteria){
		if(objeto!=null)
		  criteria.add(Restrictions.like(property,objeto))
	}
	
	
}