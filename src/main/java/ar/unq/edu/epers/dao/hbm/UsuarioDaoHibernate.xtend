package ar.unq.edu.epers.dao.hbm

import ar.unq.edu.epers.dao.UsuarioDao
import ar.unq.edu.epers.model.Usuario
import java.util.List
import org.hibernate.criterion.Restrictions
import ar.unq.edu.epers.hbmUtils.SessionManager

class UsuarioDaoHibernate implements UsuarioDao{
	
	override findAll() {
		SessionManager.getSession().createCriteria(Usuario).list as List<Usuario>
	}
	
	override findByName(String name) {
		SessionManager.getSession().createCriteria(Usuario).add(Restrictions.like("nombre",name)).list as List<Usuario>
	}
	
	override insertUsuario(Usuario usuario) {
		SessionManager.getSession().save(usuario)
	}
	
	override updateUsuario(Usuario usuario) {
		SessionManager.getSession().update(usuario)
	}
	
	override deleteUsuario(Usuario usuario) {
		SessionManager.getSession().delete(usuario)
	}
	
	override getUsuarioPorCodigoValidacion(String codigoValidacion) {
		var list=SessionManager.getSession().createCriteria(Usuario).add(Restrictions.like("codigoValidacion",codigoValidacion)).list as List<Usuario>
	     list.get(0)
	}
	
	override getUsuarioPorUsername(String userName) {
		var list=SessionManager.getSession().createCriteria(Usuario).add(Restrictions.like("userName",userName)).list as List<Usuario>
	     list.get(0)
	}
	def deleteAll(){
		SessionManager.session.createQuery("delete from "+Usuario.simpleName).executeUpdate
	}
	
}