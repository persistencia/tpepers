package ar.unq.edu.epers.dao

import ar.unq.edu.epers.model.BusquedaDeReservasCache
import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Session
import com.datastax.driver.mapping.Mapper
import com.datastax.driver.mapping.MappingManager
import java.util.Date
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class CacheDao {
	Cluster cluster
	Session session
	Mapper<BusquedaDeReservasCache> reservas
	
	new (){
		conectar
	}
	
	def createSchema(){
		session.execute("CREATE KEYSPACE IF NOT EXISTS persistenciaReservas
			WITH replication = {'class':'SimpleStrategy','replication_factor':3}")
		
		session.execute("CREATE TABLE IF NOT EXISTS persistenciaReservas.autosDisponibles (" + 
				"dia timestamp, " + 
				"ubicacion text, " +
				"autosCacheados list<text>," + 
				"PRIMARY KEY (dia, ubicacion));"
		)
		
		reservas = new MappingManager(session).mapper(BusquedaDeReservasCache)
	}
	
	def conectar(){
		cluster = Cluster.builder.addContactPoint("localhost").build()
		session = cluster.connect()
		createSchema
	}
	
	def cerrarConexion(){
		cluster.close
	}
	
	def getPorUbicacionYFecha(String ubicacion, Date dia){
		reservas.get(ubicacion,dia)
	}
	
	def saveReserva(String ubicacion, Date fecha, List<String> autos){
		var reserva = new BusquedaDeReservasCache(fecha, ubicacion, autos)
		reservas.save(reserva)
	}
	
	def deleteReserva(String ubicacion, Date fecha, String auto){
		var reservas = getPorUbicacionYFecha(ubicacion, fecha)
		if(reservas != null && reservas.patentesDeAutos.contains(auto)){
			var patentes = reservas.patentesDeAutos
			patentes.remove(auto)
			saveReserva(ubicacion,fecha,patentes)
			}
	}
	
	def clean(){
			session.execute("DROP TABLE persistenciaReservas.autosDisponibles")
			session.execute("DROP KEYSPACE persistenciaReservas")
	}

}