package ar.unq.edu.epers.dao.hbm

import ar.unq.edu.epers.hbmUtils.SessionManager
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.services.ReservaDaoHbm
import java.util.List

class GenericDao<T> {

	Class<T> modelType
	//protected SessionFactory sessionFactory

	new(Class<T> modelType) {
		this.modelType = modelType
		//this.sessionFactory=SessionManager.sessionFactory
	}

	def save(T t) {
		SessionManager.getSession.saveOrUpdate(t)
		//sessionFactory.openSession.saveOrUpdate(t)
	}

	def getAll() {
		SessionManager.getSession().createCriteria(modelType).list as List<T>
		//sessionFactory.openSession.createCriteria(modelType).list as List<T>
	}

	def delete(T t) {
		SessionManager.getSession().delete(t)
		//sessionFactory.openSession.delete(t)
	}

	def deleteAll() {
		SessionManager.session.createQuery("delete from "+modelType.simpleName).executeUpdate
		//sessionFactory.openSession.createQuery("delete from "+modelType.simpleName).executeUpdate
	}
}

class DaoProvider{
	
	static def getCategoriaDao(){
		new GenericDao(Categoria)
	}
	
	static def getAutoDao(){
		new AutoDaoHbm
	}
	
	def static reservaDao() {

		new ReservaDaoHbm(Reserva)
	}
	
	def static usuarioDao() {
		new UsuarioDaoHibernate
	}
	
}