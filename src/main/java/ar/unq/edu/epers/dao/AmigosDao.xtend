package ar.unq.edu.epers.dao

import ar.unq.edu.epers.model.Mensaje
import ar.unq.edu.epers.model.TipoDeRelacion
import ar.unq.edu.epers.model.Usuario
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.neo4j.graphdb.Direction
import org.neo4j.graphdb.DynamicLabel
import org.neo4j.graphdb.GraphDatabaseService
import org.neo4j.graphdb.Node
import org.neo4j.graphdb.RelationshipType

@Accessors
class AmigosDao {
	GraphDatabaseService graph
	
	new(GraphDatabaseService graph) {
		this.graph = graph
	}

	
	private def amigoLabel() {
		DynamicLabel.label("Persona")
	}

	private def mensajeLabel() {
		DynamicLabel.label("Mensaje")
	}
	
	def crearNodo(Usuario u) {
		val node = this.graph.createNode(amigoLabel)
		node.setProperty("nombreUsuario", u.userName)
		node.setProperty("nombre", u.nombre)
		node
	}
	
	def crearNodo(Mensaje men) {
		val node = this.graph.createNode(mensajeLabel)
		node.setProperty("mensaje", men.mensaje)		
		node
	}
	
	def eliminarNodo(Usuario u) {
		val nodo = this.getNodo(u)
		nodo.relationships.forEach[delete]
		nodo.delete
	}
	
	def getNodo(Usuario u) {
		this.getNodo(u.userName)
	}
	
	def getNodo(String uname) {
		this.graph.findNodes(amigoLabel, "nombreUsuario", uname).head
	}

	def relacionarAmigo(Usuario u1, Usuario u2, TipoDeRelacion relacion) {
		val nodo1 = this.getNodo(u1);
		val nodo2 = this.getNodo(u2);
		nodo1.createRelationshipTo(nodo2, relacion);
	}
	
	def getAmigos(Usuario usr){
		val nodoPersona = this.getNodo(usr)
		val nodoPadres = this.nodosRelacionados(nodoPersona, TipoDeRelacion.AMIGO, Direction.OUTGOING)
		nodoPadres.map[toPersona].toSet
	}
	
	protected def nodosRelacionados(Node nodo, RelationshipType tipo, Direction direccion) {
		nodo.getRelationships(tipo, direccion).map[it.getOtherNode(nodo)]
	}
	
	private def toPersona(Node nodo) {
		new Usuario => [
			nombre = nodo.getProperty("nombre") as String
			userName = nodo.getProperty("nombreUsuario") as String
		]
	}
	
	private def toMensaje(Node nodo){
		new Mensaje() => [
			mensaje = nodo.getProperty("mensaje") as String
			to = nodo.getRelationships(TipoDeRelacion.DESTINATARIO,Direction.INCOMING).head as Usuario
			from = nodo.getRelationships(TipoDeRelacion.REMITENTE,Direction.INCOMING).head as Usuario
		]
	}
	
	def void enviarMensaje(Mensaje mens){
		var nodoMensaje = crearNodo(mens)
		var nodoTo = getNodo(mens.to)
		var nodoFrom = getNodo(mens.from)
		nodoMensaje.createRelationshipTo(nodoTo, TipoDeRelacion.DESTINATARIO)
		nodoMensaje.createRelationshipTo(nodoFrom, TipoDeRelacion.REMITENTE)
		
	}
	
	def getMensajeDestinatario(String userName){
		val nodoPersona = this.getNodo(userName)
		val nodoDestinatarios = this.nodosRelacionados(nodoPersona, TipoDeRelacion.DESTINATARIO, Direction.INCOMING)
		nodoDestinatarios.map[toMensaje(it)].toSet
	}
	
	def getMensajesRemitente(String userName){
		val nodoPersona = this.getNodo(userName)
		val nodoRemitentes = this.nodosRelacionados(nodoPersona, TipoDeRelacion.REMITENTE, Direction.INCOMING)
		nodoRemitentes.map[toMensaje(it)].toSet
	}
	
	def Boolean esAmigoDe(String userName, String otroUser){
		val nodoUno=this.getNodo(userName)
		val amigos=this.nodosRelacionados(nodoUno,TipoDeRelacion.AMIGO,Direction.INCOMING).map[toPersona(it)].toList
		amigos.exists[it.userName.equals(otroUser)]
		
		
		
	}
}