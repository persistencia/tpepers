package ar.unq.edu.epers.dao

import ar.unq.edu.epers.model.Usuario
import java.util.List

/**
 * Esta interfaz define las operaciones que se utilizarán para persistir objetos en una base de datos
 */
interface UsuarioDao {
	
	def List<Usuario> findAll()
	def List<Usuario> findByName(String name)
	def Object insertUsuario(Usuario usuario)
	def void updateUsuario(Usuario usuario)
	def void deleteUsuario(Usuario usuario)
	def Usuario getUsuarioPorCodigoValidacion(String codigoValidacion)
	def Usuario getUsuarioPorUsername(String userName)
	
	
	
	
}