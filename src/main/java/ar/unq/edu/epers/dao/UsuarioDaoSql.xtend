package ar.unq.edu.epers.dao

import java.sql.Connection
import java.sql.Date
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.ArrayList
import ar.unq.edu.epers.dao.UsuarioDao
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.exceptions.MiSQLException
import java.sql.SQLException

/**
 * Implementa los métodos para persistir una base de datos
 */
class UsuarioDaoSql implements UsuarioDao{
	val String dbNameProd="Rentauto"
	val String dbNameTest="RentautoTest"
	
	/**
	 * Busca en la base de datos y devuelve todos los usuarios
	 */
	override findAll() {
		var Connection conn
		var PreparedStatement ps
		var ArrayList<Usuario> usuarios
		try {
			conn = this.getConnection
			
			ps = conn.prepareStatement ("SELECT * FROM Usuario")
			val ResultSet rs = ps.executeQuery()
			
			usuarios = this.procesarFila(rs)
			
			if(rs!=null)
				rs.close()
				
		} catch (Exception e){
			  throw new MiSQLException("No existen usuarios en la base de datos", e)
			}
			finally {
			if(conn!= null)
				conn.close()
			if(ps!=null)
				ps.close()
		}
		usuarios
	}
	
	/**
	 * Busca en la base de datos y devuelve los usuarios con el nombre enviado por parametro
	 */
	override findByName(String name) { 
		var Connection conn
		var PreparedStatement ps
		var ArrayList<Usuario> usuarios
		try {
			conn = this.getConnection
			
			ps = conn.prepareStatement ("SELECT * FROM Usuario WHERE Nombre=?")
			ps.setString(1, name)
			val ResultSet rs = ps.executeQuery()
			usuarios = this.procesarFila(rs)
			if(rs!=null)
				rs.close()
			
		}catch (Exception e){
			  throw new MiSQLException("No existen usuarios con el nombre " + name + " en la base de datos", e)
			}finally {
			if(conn!= null)
				conn.close()
			if(ps!=null)
				ps.close()
		}
		usuarios
	}
	
	/**
	 * Agrega un usuario nuevo a la base de datos
	 */
	override insertUsuario(Usuario usuario) {
		var Connection conn = null
		var PreparedStatement ps = null
		var Boolean ret
		try {
			conn = this.getConnection
			ps = conn.prepareStatement("INSERT INTO Usuario(NombreDeUsuario,Password, Nombre, Apellido, Email, FechaNacimiento, Validado, CodigoValidacion)
										VALUES (?,?,?,?,?,?,?,?)")
			ps.setString(1, usuario.userName)
			ps.setString(2, usuario.password)
			ps.setString(3, usuario.nombre)
			ps.setString(4, usuario.apellido)
			ps.setString(5, usuario.direccionMail)
			val Date fecha = new Date(usuario.fechaNacimiento.getTime)
			ps.setDate(6, fecha)
			ps.setBoolean(7,false)
			ps.setString(8, usuario.codigoValidacion)
			ps.execute
			var PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM Usuario WHERE NombreDeUsuario=?;")
			ps2.setString(1, usuario.userName)
			var ResultSet rs = ps2.executeQuery
			rs.next
			var usrNameSql=rs.getString("NombreDeUsuario")
			ret = usrNameSql.equals(usuario.userName)
			if(rs!=null)
				rs.close()
			
					
		}catch (Exception e){
			  throw new MiSQLException("No se puede agregar al usuario " + usuario.userName + " a la base de datos",e)
			}finally {
			if(conn!= null)
				conn.close
			if(ps!=null)
				ps.close
		}
		ret	
	}
	
	/**
	 * Actualiza un usuario que es pasado por parámetro y persiste los cambios en la base de datos
	 */
	override updateUsuario(Usuario usuario) {
		var Connection conn
		var PreparedStatement ps
		var Usuario usr
		try {
			conn = this.getConnection
			ps = conn.prepareStatement("UPDATE Usuario SET Nombre=?, Apellido=?, Email=?, Validado=?,
										CodigoValidacion=?, Password=? WHERE NombreDeUsuario=?;")
			ps.setString(1, usuario.nombre)
			ps.setString(2, usuario.apellido)
			ps.setString(3, usuario.direccionMail)
			ps.setBoolean(4, usuario.validated)
			ps.setString(5, usuario.codigoValidacion)
			ps.setString(6, usuario.password)
			ps.setString(7, usuario.userName)
			ps.execute
			ps = conn.prepareStatement("SELECT * FROM Usuario WHERE NombreDeUsuario=?")
			ps.setString(1, usuario.userName);
			val ResultSet rs = ps.executeQuery()
			usr = procesarUsuario(rs)
			usr.setCodigoValidacion(rs.getString("CodigoValidacion"))

			if(rs!=null)
			  rs.close()
		}catch (Exception e){
			  throw new MiSQLException("No se puede actualizar al usuario " + usuario.userName, e)
			}finally {
			if(conn!= null)
				conn.close()
			if(ps!=null)
				ps.close()
		}
		usuario.nombre.equals(usr.nombre) || usuario.apellido.equals(usuario.apellido) || 
			usuario.direccionMail.equals(usr.direccionMail) || (usuario.validated || usr.validated) ||
			usuario.codigoValidacion.equals(usr.codigoValidacion)
	}
	
	/**
	 * Elimina de la base de datos el usuario enviado por parámetro
	 */
	override deleteUsuario(Usuario usuario) {
		var Connection conn
		var PreparedStatement ps
		var ResultSet rs
		try {
			conn = this.getConnection()
			ps = conn.prepareStatement ("DELETE FROM Usuario WHERE NombreDeUsuario = ?;")
			ps.setString(1, usuario.userName)
			ps.execute()
			ps = conn.prepareStatement ("SELECT NombreDeUsuario FROM Usuario WHERE NombreDeUsuario =?;")
			ps.setString(1,usuario.userName)
			rs = ps.executeQuery()
			
			if(rs!=null)
			  rs.close()
			
		}catch (Exception e){
			  throw new MiSQLException("No se puede eliminar al usuario " + usuario.userName, e)
			}finally {
			if(conn!= null)
				conn.close()
			if(ps!=null)
				ps.close()
		}
		
	}
	
	/**
	 * Busca en la base de datos y devuelve el usuario con el código de validación que tiene asociado
	 * y que es pasado por parámetro
	 */
	override getUsuarioPorCodigoValidacion(String codigoValidacion) {
		var Connection conn
		var PreparedStatement ps
		var Usuario usr
		try {
			conn = this.getConnection()
			ps = conn.prepareStatement ("SELECT * FROM Usuario WHERE codigoValidacion=?;")
			ps.setString(1,codigoValidacion)
			val ResultSet rs = ps.executeQuery()
			usr = procesarUsuario(rs)
			
			
			if(rs!=null)
			  rs.close()
		}catch (Exception e){
			  throw new MiSQLException("No existe un usuario con el código de validación " + codigoValidacion, e)
			}finally {
			if (ps!=null)
				ps.close()
			if (conn!=null)
				conn.close()
		}
		usr
	}
		
	/**
	 * Busca en la base de datos un usuario  con el nombre de usuario pasado por parámetro y lo devuelve
	 */
	override getUsuarioPorUsername(String usrName) {
		var Connection conn 
		var PreparedStatement ps
		var Usuario usr
		try{
			conn = this.getConnection()
			ps = conn.prepareStatement("SELECT * FROM Usuario WHERE nombreDeUsuario=?;")
			ps.setString(1, usrName)
			val ResultSet rs = ps.executeQuery()
			usr = procesarUsuario(rs)
			if(rs!=null)
				rs.close()
		
		}catch (Exception e){
			  throw new MiSQLException("No existe un usuario con el nombre " + usrName, e)
			}finally{
			if(ps != null)
				ps.close()
			if(conn != null)
				conn.close()
		}
		usr
	}
	
	/**
	 * Devuelve un usuario inicializado con los atributos correspondientes del ResultSet pasado por parámetro
	 */
	def private procesarUsuario(ResultSet rs){
		var Usuario usr
		if(rs.next)
			  usr = construirUsuario(rs)
		usr
	}
	
	/**
	 * Devuelve una lista con todos los usuarios del ResultSet pasado por parámetro
	 */
	def private procesarFila(ResultSet rs){
		var usuarios=new ArrayList
		while (rs.next ){
				 var Usuario usr = construirUsuario(rs)
				usuarios.add(usr)
			}
		usuarios
	}
	
	def private construirUsuario(ResultSet rs){
		var usr =new Usuario =>[
						userName = rs.getString("NombreDeUsuario")
						nombre = rs.getString("Nombre")
						apellido = rs.getString("Apellido")
						password = rs.getString("Password")
						direccionMail = rs.getString("Email")
						fechaNacimiento = rs.getDate("FechaNacimiento")
						validated = rs.getBoolean("Validado")
						setCodigoValidacion(rs.getString("CodigoValidacion"))
						]
		usr
	}
	
	/**
	 * Levanta una bd con el nombre pasado por parametro creandole la tabla Usuario
	 */
	def levantarDB(String nombreDB){
		var Connection conn
		var PreparedStatement ps
		try {
			conn = this.getConnection()
			ps = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS " + nombreDB)
			ps.execute()
			ps.executeUpdate("USE " + nombreDB)
			ps.executeUpdate(
				"CREATE TABLE IF NOT EXISTS " + nombreDB + ".Usuario (NombreDeUsuario VARCHAR(100)  NOT NULL,Password VARCHAR(100) NOT NULL,Nombre VARCHAR(100)  NOT NULL,ApellidO VARCHAR(100)  NOT NULL,Email VARCHAR(100)  NOT NULL,FechaNacimiento DATE  NOT NULL,Validado BIT NOT NULL,CodigoValidacion VARCHAR(150),PRIMARY KEY (NombreDeUsuario))"
			)
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}		
	}
	
	/**
	 * Elimina la bd con el nombre pasado por parametro
	 */
	def dropearDB(String nombreDB){
		var Connection conn
		var PreparedStatement ps
		try {
			conn = this.getConnection()
			ps = conn.prepareStatement("DROP DATABASE " + nombreDB)
			ps.execute()
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}
	}
	
	/**
	 * Establece la conexion con la base de datos y selecciona el driver apropiado.
	 * Arroja una excepcion cuando se produce un error
	 */
	def private Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver")
		return DriverManager.getConnection("jdbc:mysql://localhost/"+dbNameProd+"?user=root&password=root")
	}	
	
}
