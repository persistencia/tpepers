package ar.unq.edu.epers.dao

import ar.unq.edu.epers.model.Auto
import java.util.List

interface AutoDao {
	def Auto getPorNroPatente(String patente)
	def void save (Auto auto)
	def List<Auto> getAll()
	def void delete(Auto auto)
	def void deleteAll()
}