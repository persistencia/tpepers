package ar.unq.edu.epers.mailing

import ar.unq.edu.epers.exceptions.EnviarMailException

/**
 * Esta interfaz es la encargada de enviar los mails.
 */
interface EnviadorDeMails {
	
	/**
	 * Se encarga de realizar el envío de un mail. Si existiera algun error lanza una excepción
	 */
	def void enviarMail(Mail m)throws EnviarMailException

	
}