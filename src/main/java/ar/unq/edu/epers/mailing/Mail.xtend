package ar.unq.edu.epers.mailing

import org.eclipse.xtend.lib.annotations.Accessors

/**
 * Representa a un Mail con todos sus parametros (cuerpo, asunto, destinatario, remitente)
 * No realiza ninguna accion en particular 
*/ 

@Accessors class Mail {
	

	var String body
	var String subject
	var String to
	var String from
	
	new() {
	}
	
	
}