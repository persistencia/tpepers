package ar.unq.edu.epers.hbmUtils

interface Runner {
	
	def <T> T run(ServiceCommand<T> s)
}