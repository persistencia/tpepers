package ar.unq.edu.epers.hbmUtils

import java.util.concurrent.Callable

interface ServiceCommand<T> extends Callable<T> {
	override def T call()
}