package ar.unq.edu.epers.exceptions

import java.lang.Exception
import org.eclipse.xtend.lib.annotations.Accessors
import ar.unq.edu.epers.mailing.Mail

/**
 * Esta clase modela la excepción que se tira cuando el mail no se puede mandar y lo conserva
 */
@Accessors class EnviarMailException extends Exception {
	var Mail mail
}