package ar.unq.edu.epers.exceptions

class MiSQLException extends Exception{
	
	new(String message, Throwable tr) {
		super(message, tr)
	}
	
}