CREATE SCHEMA IF NOT EXISTS `Rentauto`;

USE `Rentauto`;

CREATE TABLE IF NOT EXISTS `Rentauto`.`Usuario` (
  `NombreDeUsuario` VARCHAR(100)  NOT NULL,
  `Nombre` VARCHAR(100)  NOT NULL,
  `Apellido` VARCHAR(100)  NOT NULL,
  `Email` VARCHAR(100)  NOT NULL,
  `FechaNacimiento` DATE  NOT NULL,
  `Validado` BIT NOT NULL,
  `CodigoValidacion` VARCHAR(150),
  PRIMARY KEY (`NombreDeUsuario`)
);
