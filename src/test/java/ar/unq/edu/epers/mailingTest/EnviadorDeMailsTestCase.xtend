package ar.unq.edu.epers.mailingTest

import ar.unq.edu.epers.mailing.EnviadorDeMails
import ar.unq.edu.epers.mailing.Mail
import ar.unq.edu.epers.exceptions.EnviarMailException
import org.junit.Before
import org.junit.Test

import static org.mockito.Mockito.*

/*
 * Se testea el funcionamiento de la interface probando que un mail se "envia" y que la excepción
 * ocurre oportunamente
 */
class EnviadorDeMailsTestCase {
	package Mail mail
	package EnviadorDeMails enviador

	@Before def void setUp() throws Exception {
		enviador = mock(EnviadorDeMails)
	}

	@Test def void testEnviarMailCorrecto() throws EnviarMailException {
		enviador.enviarMail(mail)
		verify(enviador).enviarMail(mail)
	}

	@Test(expected=EnviarMailException) def void testEnviarMailConExcepcion() throws EnviarMailException {
		doThrow(new EnviarMailException()).when(enviador).enviarMail(mail)
		enviador.enviarMail(mail)
	}

}
