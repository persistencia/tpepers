package ar.unq.edu.epers.daoTest

import ar.unq.edu.epers.hbmUtils.SessionManager
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import ar.unq.edu.epers.dao.hbm.DaoProvider
import ar.unq.edu.epers.dao.hbm.CategoriaDaoHbm
import ar.unq.edu.epers.model.Deportivo
import ar.unq.edu.epers.dao.hbm.UsuarioDaoHibernate
import ar.unq.edu.epers.model.Usuario
import java.util.Date
import ar.unq.edu.epers.model.Reserva
import java.util.List

class UsuarioDaoHibernateTestCase {
	var Usuario usr
	var Usuario usr2
	var UsuarioDaoHibernate dao 
	var List<Reserva> list
	
	@Before
	def  void setUp(){
		usr = new Usuario => [
			nombre = "Federico"
			apellido = "Mancuello"
			userName = "superMancu"
			password = "soyDios123"
			direccionMail = "superMancu@gmail.com"
			fechaNacimiento = new Date()
			codigoValidacion = "123456789"
			reservas = list
		]
		usr2 = new Usuario => [			
			nombre = "Nicolas"
			apellido = "Tagliafico"
			userName = "super3"
			password = "masSosHorrible"
			direccionMail = "taglia@gmail.com"
			fechaNacimiento = new Date()
			codigoValidacion = "123456788"
			reservas = list
		]
		dao= new UsuarioDaoHibernate 
	}
	
	@After
	def  void tearDown(){
		SessionManager.runInSession[|
		dao.deleteAll
		usr=null
		usr2=null
		dao= null
		]
		
	}
	
	@Test def void testGetAll(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			var Integer expected = 1
			var Integer actual = dao.findAll.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	@Test def void testGetByCodigoValidacion(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			var Usuario buscado = dao.getUsuarioPorCodigoValidacion("123456789")
			assertEquals(usr, buscado)
			1
			
		]
		
	}
	
	@Test def void testGetPorUsername(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			var Usuario buscado = dao.getUsuarioPorUsername("superMancu")
			assertEquals(usr, buscado)
			1
			
		]
		
	}
	
	@Test def void testDelete(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			dao.insertUsuario(usr2)
			var Integer expected = 1
			dao.deleteUsuario(usr2)
			var Integer actual = dao.findAll.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	@Test def void testFindByNameYUpdate(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			dao.insertUsuario(usr2)
			var Integer expected = 2
			var Integer actual = dao.findAll.size
			assertEquals(expected, actual)
			usr2.nombre = "soyUnLeon"
			dao.updateUsuario(usr2)
			expected = 1
			actual = dao.findByName("soyUnLeon").size
			assertEquals(expected,actual)
			1
			
		]
		
	}
	
		@Test def void testDeleteAll(){
		SessionManager.runInSession[|
			dao.insertUsuario(usr)
			dao.insertUsuario(usr2)
			var Integer expected = 0
			dao.deleteAll()
			var Integer actual = dao.findAll.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	
}