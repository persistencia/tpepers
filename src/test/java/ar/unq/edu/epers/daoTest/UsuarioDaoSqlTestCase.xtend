package ar.unq.edu.epers.daoTest

import ar.unq.edu.epers.dao.UsuarioDao
import ar.unq.edu.epers.dao.UsuarioDaoSql
import ar.unq.edu.epers.model.CodeGenerator
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.sqlRunner.ScriptRunner
import java.io.BufferedReader
import java.io.FileReader
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.SQLException
import java.util.ArrayList
import java.util.Date
import java.util.List
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*

class UsuarioDaoSqlTestCase {
	package UsuarioDao dao
	package Connection conn
	package PreparedStatement ps
	package Usuario usr1
	package Usuario usr2
	package Usuario usr3

	@Before def void setUp() throws Exception {
		usr1 = new Usuario=>[
			setNombre("Mario")
			setApellido("Fontenero")
			setUserName("Mario64")
			setPassword("Mariof1964")
			setDireccionMail("mario@gmail.com")
			setFechaNacimiento(new Date(489451))
		]
		usr1.setCodigoValidacion(CodeGenerator.newCode(usr1))
		usr2 = new Usuario=>[
			setNombre("Luis")
			setApellido("Fontanero")
			setUserName("Luis64")
			setPassword("luisitoelmascapo123")
			setDireccionMail("luis@gmail.com")
			setFechaNacimiento(new Date(489452))
		]
		usr2.setCodigoValidacion(CodeGenerator.newCode(usr2))
		usr3 = new Usuario=>[
			setNombre("Luis")
			setApellido("Miguel")
			setUserName("Luismi")
			setPassword("luismi123")
			setDireccionMail("luism@gmail.com")
			setFechaNacimiento(new Date(489452))
		]
		usr3.setCodigoValidacion(CodeGenerator.newCode(usr3))
		try {
			conn = this.getConnection()
			var ScriptRunner runner = new ScriptRunner(conn, false, false)
            runner.runScript(new BufferedReader(new FileReader("crearTableUsuarios.sql")));
		
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}
		dao = new UsuarioDaoSql()
	}

	@After def void tearDown() throws Exception {
		try {
			conn = this.getConnection()
			var ScriptRunner runner = new ScriptRunner(conn, false, false)
            runner.runScript(new BufferedReader(new FileReader("borrarTablaUsuarios.sql")));
		
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}
	}

	@Test def void testFindAll() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr3)
		var List<Usuario> registrados = dao.findAll()
		var List<String> nombres = new ArrayList<String>()
		for (Usuario u : registrados) {
			nombres.add(u.getUserName())
		}
		assertTrue(nombres.contains("Luis64"))
		assertTrue(nombres.contains("Mario64"))
		assertTrue(nombres.contains("Luismi"))
		assertEquals(registrados.size(), 3)
	}

	@Test def void testFindByName() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr3)
		var List<Usuario> marios = dao.findByName("Mario")
		var List<Usuario> luises = dao.findByName("Luis")
		assertEquals(marios.size(), 1)
		assertEquals(luises.size(), 2)
	}

	@Test def void testGetUsuarioPorUsername() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr3)
		var Usuario Mario64 = dao.getUsuarioPorUsername("Mario64")
		var Usuario Luis64 = dao.getUsuarioPorUsername("Luis64")
		var Usuario luismi = dao.getUsuarioPorUsername("luismi")
		assertEquals(Mario64.getUserName(), usr1.getUserName())
		assertEquals(Luis64.getUserName(), usr2.getUserName())
		assertEquals(luismi.getUserName(), usr3.getUserName())
	}

	@Test def void testInsertUsuario() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr3)
		dao.insertUsuario(usr2)
		var List<Usuario> usuariosRegistrados = dao.findAll()
		assertEquals(usuariosRegistrados.size(), 3)
	}

	@Test def void TestDeleteUsuario() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr3)
		var List<Usuario> usuariosRegistrados = dao.findAll()
		assertEquals(usuariosRegistrados.size(), 3)
		dao.deleteUsuario(usr3)
		var List<Usuario> usuariosActualizados = dao.findAll()
		assertEquals(usuariosActualizados.size(), 2)
	}

	@Test def void TestUpdateUsuario() {
		dao.insertUsuario(usr1)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr3)
		var String passwordVieja = usr1.getPassword()
		usr1.setPassword("Hola123")
		dao.updateUsuario(usr1)
		var Usuario usr1Updated = dao.getUsuarioPorUsername("Mario64")
		assertNotEquals(usr1Updated.getPassword(), passwordVieja)
		assertEquals(usr1Updated.getPassword(), "Hola123")
	}

	@Test def void TestGetUsuarioPorCodigoValidacion() {
		dao.insertUsuario(usr3)
		dao.insertUsuario(usr2)
		dao.insertUsuario(usr1)
		var Usuario usuarioPorCod1 = dao.getUsuarioPorCodigoValidacion(CodeGenerator.newCode(usr3))
		assertEquals(usuarioPorCod1.getUserName(), usr3.getUserName())
	}

	def private Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver")
		return DriverManager.getConnection("jdbc:mysql://localhost/?user=root&password=root")
	}

}
