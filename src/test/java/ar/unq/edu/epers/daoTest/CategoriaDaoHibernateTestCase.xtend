package ar.unq.edu.epers.daoTest

import ar.unq.edu.epers.dao.hbm.AutoDaoHbm
import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria 
import ar.unq.edu.epers.model.Familiar
import ar.unq.edu.epers.model.Ubicacion
import ar.unq.edu.epers.hbmUtils.SessionManager
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import ar.unq.edu.epers.dao.hbm.DaoProvider
import ar.unq.edu.epers.dao.hbm.CategoriaDaoHbm
import ar.unq.edu.epers.model.Deportivo

class CategoriaDaoHibernateTestCase {
	var Categoria cat
	var Categoria cat2
	var CategoriaDaoHbm dao 
	
	@Before
	def  void setUp(){
		cat = new Deportivo => [nombre="altoDepor"]
		cat2 = new Deportivo => [nombre="unBuenDepor"]
		dao= new CategoriaDaoHbm 
	}
	
	@After
	def  void tearDown(){
		SessionManager.runInSession[|
		dao.deleteAll
		cat=null
		cat2=null
		dao= null
		]
		
	}
	
	@Test def void testGetAll(){
		SessionManager.runInSession[|
			dao.save(cat)
			var Integer expected = 1
			var Integer actual = dao.all.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	@Test def void testDelete(){
		SessionManager.runInSession[|
			dao.save(cat)
			dao.save(cat2)
			var Integer expected = 1
			dao.delete(cat2)
			var Integer actual = dao.all.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
		@Test def void testDeleteAll(){
		SessionManager.runInSession[|
			dao.save(cat)
			dao.save(cat2)
			var Integer expected = 0
			dao.deleteAll()
			var Integer actual = dao.all.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	
}