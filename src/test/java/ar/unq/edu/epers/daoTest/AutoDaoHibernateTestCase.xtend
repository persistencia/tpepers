package ar.unq.edu.epers.daoTest

import ar.unq.edu.epers.dao.hbm.AutoDaoHbm
import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Familiar
import ar.unq.edu.epers.model.Ubicacion
import ar.unq.edu.epers.hbmUtils.SessionManager
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import ar.unq.edu.epers.dao.hbm.DaoProvider

class AutoDaoHibernateTestCase {
	
	var Auto a1
	var Auto a2
	var Auto a3
	var AutoDaoHbm dao 
	var Categoria cat
	
	@Before
	def  void setUp(){
		cat = new Familiar => [nombre="pepe"]
		a1=new Auto("peugeot","504",1994,"abc123",cat ,30000.0,new Ubicacion("Quilmes"))
		a2=new Auto("Renault","4",1984,"dsa674", cat,12000.0,new Ubicacion("Florencio Varela"))
		a3=new Auto("Suzuki","fun",2005,"hbm234", cat,60000.0,new Ubicacion("Capital Federal"))
		dao= new AutoDaoHbm 
	}
	
	@After
	def  void tearDown(){
		SessionManager.runInSession[|
		dao.deleteAll
		a1=null
		a2=null
		a3=null
		dao= null
		]
		
	}
	
	

	@Test def void testSave(){
		SessionManager.runInSession[|
			dao.save(a1)
			var Integer expected = 1
			var Integer actual = dao.all.size
			assertEquals(expected, actual)
			1
			
		]
		
	} 
	
	@Test def void testGetAll(){
		SessionManager.runInSession[|
			dao.save(a1)
			dao.save(a2)
			var Integer expected = 2
			var Integer actual = dao.all.size
			assertEquals(expected, actual)
			1
			
		]
		
	}
	
	@Test def void testGetPorNroPatente(){
		//DaoProvider.categoriaDao.all
		SessionManager.runInSession[|
			dao.save(a1)
			dao.save(a2)
			var Auto expected = a1
			var Auto actual = dao.getPorNroPatente("abc123")
			assertEquals(expected.año, actual.año)
			assertEquals(expected.categoria, actual.categoria)
			assertEquals(expected.costoBase, actual.costoBase)
			assertEquals(expected.marca, actual.marca)
			assertEquals(expected.modelo, actual.modelo)
			
			
			expected = a2
			actual = dao.getPorNroPatente("dsa674")
			assertEquals(expected.año, actual.año)
			assertEquals(expected.categoria, actual.categoria)
			assertEquals(expected.costoBase, actual.costoBase)
			assertEquals(expected.marca, actual.marca)
			assertEquals(expected.modelo, actual.modelo)
			1
			
		]
		
	}
	
	
}