package ar.unq.edu.epers.modelTest

import ar.unq.edu.epers.model.CodeGenerator
import ar.unq.edu.epers.model.Usuario
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import static org.mockito.Mockito.*

class CodeGeneratorTestCase {
	package Usuario usr
	package CodeGenerator generador

	@Before def void setUp() throws Exception {
		usr = mock(Usuario)
		when(usr.getUserName()).thenReturn("mrRobot")
		when(usr.getNombre()).thenReturn("Elliot")
		when(usr.getPassword()).thenReturn("asge_57.-egtq")
		when(usr.getDireccionMail()).thenReturn("eAlderson@gmail.com")
		generador = new CodeGenerator()
	}

	@Test def void testNewCode() {
		var String codigo = CodeGenerator.newCode(usr)
		var String expected = "mrRobotasge_57.-egtq619"
		assertEquals(codigo, expected)
	}

}
