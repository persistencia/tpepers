package ar.unq.edu.epers.modelTest

import ar.unq.edu.epers.model.Usuario
import java.util.Date
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*

class UsuarioTestCase {
	package Usuario jose

	@Before def void setUp() throws Exception {
		jose = new Usuario()
		jose.setNombre("Jose")
		jose.setApellido("Perez")
		jose.setUserName("JP_cuervo")
		jose.setPassword("soyJose4321")
		jose.setDireccionMail("perezj@outlook.com")
		jose.setFechaNacimiento(new Date())
	}

	@Test def void testValidatePasswordConPasswordCorrecta() {
		assertTrue(jose.validatePassword("soyJose4321"))
	}

	@Test def void testValidatePasswordConPasswordIncorrecta() {
		assertFalse(jose.validatePassword("soyjose4321"))
	}

	@Test def void testValidate() {
		jose.validate()
		assertTrue(jose.getValidated())
	}

}
