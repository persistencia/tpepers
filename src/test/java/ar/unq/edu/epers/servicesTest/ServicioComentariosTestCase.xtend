package ar.unq.edu.epers.servicesTest

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Calificacion
import ar.unq.edu.epers.model.Familiar
import ar.unq.edu.epers.model.Ubicacion
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.model.Visibilidad
import ar.unq.edu.epers.services.ServicioAmigos
import ar.unq.edu.epers.services.ServicioComentarios
import org.junit.Before
import org.junit.Test
import static org.junit.Assert.*
import org.junit.After

class ServicioComentariosTestCase {
	
	Usuario u
	Usuario u2
	Usuario u3
	Auto auto
	Ubicacion quilmes
	ServicioAmigos sa
	ServicioComentarios sum
	
	@Before
	def void setup(){
		quilmes = new Ubicacion("Quilmes")
		auto = new Auto("Chevrolet", "Camaro", 2004, "CSI782", new Familiar, 7400000.0, quilmes)
		u=new Usuario =>[
			nombre="Pedro"
			userName="pepito"
		]
		u2=new Usuario =>[
			nombre="Juan"
			userName="juanchi"
		]
		u3 = new Usuario =>[
			nombre = "Jose"
			userName = "josex"
		]
		sa=new ServicioAmigos
		sum= new ServicioComentarios
	    sa.crearUsuario(u)
		sa.crearUsuario(u2)
		sa.crearUsuario(u3)
		sa.amigoDe(u,u2)
		sa.amigoDe(u2,u)
		}
		
		@After
		def void tearDown(){
			sum.purgeDB
		}
		
		@Test
		def void testCalificar(){
			sum.calificar(auto, u, "Alta tracción tiene el cacharro", Calificacion.EXCELENTE, Visibilidad.PRIVADO)
			var expected = sum.cantCalificaciones
			assertEquals(expected, 1)
		}
		
		@Test 
		def void verMiPerfilTest(){ 
			sum.calificar(auto, u, "Le falta aire acondicionado, pero el resto esta perfecto", Calificacion.BUENO, Visibilidad.PRIVADO)
			var expected= 1
			var actual = sum.verPerfil(u,u).size
			assertEquals(expected, actual)
		}
		
		@Test
		def void verPerfilAmigoTest(){
			sum.calificar(auto, u, "Me gusto", Calificacion.EXCELENTE, Visibilidad.SOLO_AMIGOS)
			sum.calificar(auto, u, "Buena dirección", Calificacion.BUENO, Visibilidad.PRIVADO)
			var expected= 1
			var actual = sum.verPerfil(u2,u).size
			assertEquals(expected, actual)
		}
		
		@Test
		def void verPerfilNoAmigoTest(){
			//Usuario U no es amigo de U3
			sum.calificar(auto, u3, "me gusto", Calificacion.EXCELENTE, Visibilidad.SOLO_AMIGOS)
			sum.calificar(auto, u3, "No quiero decir nada malo", Calificacion.EXCELENTE, Visibilidad.PUBLICO)
			var expected= 1
			var actual= sum.verPerfil(u, u3).size
			assertEquals(expected, actual)
		}
		
		@Test
		def void verPerfilPublico(){
			sum.calificar(auto, u2, "me gusto", Calificacion.EXCELENTE, Visibilidad.PUBLICO)
			var expected= 1
			var actual= sum.verPerfil(u, u2).size
			assertEquals(expected, actual)
			
		}
}