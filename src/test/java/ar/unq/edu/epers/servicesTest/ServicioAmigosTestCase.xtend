package ar.unq.edu.epers.servicesTest

import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.services.ServicioAmigos
import org.junit.After
import org.junit.Before
import org.junit.Test
import static org.junit.Assert.*

class UsuarioServiceTest {
	Usuario u
	Usuario u2
	Usuario u3
	Usuario u4
	ServicioAmigos sa
	
	@Before
	def void setup(){
		u=new Usuario =>[
			nombre="pepe"
			userName="pepito"
		]
		u2=new Usuario =>[
			nombre="juan"
			userName="juanchi"
		]
		u3=new Usuario =>[
			nombre="carlos"
			userName="carlitos"
		]
		u4=new Usuario =>[
			nombre="pepe"
			userName="pepito"
		]
		sa=new ServicioAmigos
		sa.crearUsuario(u)
		sa.crearUsuario(u2)
		sa.crearUsuario(u3)
		sa.amigoDe(u,u3)
		sa.amigoDe(u3,u2)
		sa.amigoDe(u2,u3)
		sa.amigoDe(u2,u)
		   	
		
	}
	@After
	def void tearDown(){
		sa.eliminarUsuario(u)
		sa.eliminarUsuario(u2)
		sa.eliminarUsuario(u3)
	}
	
	@Test
	def void testGetAmigos(){
		var amigos=sa.getAmigos(u)
		var amigos2=sa.getAmigos(u2)
		var amigos3=sa.getAmigos(u3)
		assertEquals(amigos.size,1)
		assertEquals(amigos2.size,2)
		assertEquals(amigos3.size,1)
	}
	
	@Test
	def void testGetConectados(){
		var amigos=sa.getConectados(u)
		var amigos2=sa.getConectados(u2)
		var amigos3=sa.getConectados(u3)
		
		assertEquals(2, amigos.size)
		assertEquals(2, amigos2.size)
		assertEquals(2,amigos3.size)
	}
	
	@Test
	def void testAnadirAmigo(){
		sa.amigoDe(u,u2)
		var amigos=sa.getAmigos(u)
		assertEquals(amigos.size,2)
		
	}
	
	@Test
	def void testMensajesEnviados(){
		sa.mandarMensaje(u,u2,"Que hay de nuevo viejo")
		var remitentes = sa.getMensajesDestinatario(u.userName)
		var destinatarios = sa.getMensajesRemitente(u2.userName)
		assertEquals(1, remitentes.size)
		assertEquals(1, destinatarios.size)
	}
	
}