package ar.unq.edu.epers.servicesTest

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.Categoria
import ar.unq.edu.epers.model.Familiar
import ar.unq.edu.epers.model.Reserva
import ar.unq.edu.epers.model.Ubicacion
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.services.ServicioAutoHbm
import ar.unq.edu.epers.services.ServicioReservaHbm
import java.util.Date
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import ar.unq.edu.epers.hbmUtils.SessionManager

class ServicioAutoTestCase {
	
	var Auto a1
	var Auto a2
	var Categoria cat
	var ServicioAutoHbm serv
	var Ubicacion quil
	var Ubicacion vare
	
	@Before
	def  void setUp(){
		cat = new Familiar => [nombre="pepe"]
		quil = new Ubicacion("Quilmes")
		vare = new Ubicacion("Varela")
		serv = new ServicioAutoHbm
		a1=new Auto("peugeot","504",1994,"abc123",cat ,30000.0,quil)
		a2=new Auto("Renault","4",1984,"dsa674", cat,12000.0,vare)
		serv.guardarAuto(a1)
		serv.guardarAuto(a2)
		
	}
	@After
	def  void tearDown(){
		SessionManager::resetSessionFactory()
		
	}
	

	
	@Test def void testGetAutosConParametrosSinReservas(){
			var Integer expected = 0
			var Integer actual = serv.disponiblesEn(quil,quil,new Date(),new Date(),cat).size 
			assertEquals(expected, actual)
	}
	
	@Test def void testGetAutosConParametrosConUnaReserva(){
		val user = new Usuario()=>[
			nombre="Jose"
			apellido="Perez"
			userName="JP"
			password="a"
			direccionMail="j"
			fechaNacimiento=new Date(2342)
		]
		var servRes = new ServicioReservaHbm
		var res = new Reserva()=>[
			numeroSolicitud=1
			origen=quil
			destino=vare
			inicio=new Date
			fin=new Date
			auto=a1
			usuario=user
		]
		servRes.guardar(res)
		var expected = 1
		var actual = serv.disponiblesEn(quil,vare,new Date(), new Date(), cat).size
		assertEquals (expected, actual)
	}
	
	
	 @Test def void testDisponibleEnUbicacionYDiaConAutoDisponible(){
			var Integer expected = 1
			var disponiblesEnUbicacion = serv.disponiblesEnUbicacionYDia(quil,new Date).size
		
			assertEquals(expected, disponiblesEnUbicacion)
		
	}
	
	@Test def void testDisponibleEnUbicacionYDiaSinAutoDisponible(){
		var expected = 0
		var ubi = new Ubicacion("Bernal")
		var actual= serv.disponiblesEnUbicacionYDia(ubi, new Date).size
		
		assertEquals(expected,actual)
	}
	
	
}