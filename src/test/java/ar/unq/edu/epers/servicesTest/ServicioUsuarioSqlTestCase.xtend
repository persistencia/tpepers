package ar.unq.edu.epers.servicesTest

import ar.unq.edu.epers.dao.UsuarioDaoSql
import ar.unq.edu.epers.exceptions.EnviarMailException
import ar.unq.edu.epers.mailing.EnviadorDeMails
import ar.unq.edu.epers.mailing.Mail
import ar.unq.edu.epers.model.CodeGenerator
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.services.NuevaPasswordInválida
import ar.unq.edu.epers.services.ServicioUsuarioSql
import ar.unq.edu.epers.services.UsuarioNoExiste
import ar.unq.edu.epers.services.UsuarioYaExisteException
import ar.unq.edu.epers.services.ValidacionException
import ar.unq.edu.epers.sqlRunner.ScriptRunner
import java.io.BufferedReader
import java.io.FileReader
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.SQLException
import java.util.Date
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import static org.mockito.Matchers.*
import static org.mockito.Mockito.*

class ServicioUsuarioSqlTestCase {
	ServicioUsuarioSql servicio
	UsuarioDaoSql dao
	Connection conn
	PreparedStatement ps
	EnviadorDeMails em
	Usuario usr1
	Usuario usr2
	Usuario usr3
	Usuario usr4
	
	@Before def void setUp() throws Exception {
		usr1 = new Usuario =>[
			setNombre("Elliot")
			setApellido("Pena")
			setUserName("mrRobot")
			setPassword("asge_57.-egtq")
			setDireccionMail("eAlderson@gmail.com")
			setFechaNacimiento(new Date())
		]
		usr1.setCodigoValidacion(CodeGenerator.newCode(usr1))
		
		usr2 = new Usuario =>[
			setNombre("Elliot")
			setApellido("Alderson")
			setUserName("mrRobot")
			setPassword("asge_57.-egtq")
			setDireccionMail("eAlderson@gmail.com")
			setFechaNacimiento(new Date())
		]
		usr2.setCodigoValidacion(CodeGenerator.newCode(usr2))
		
		usr3 = new Usuario =>[
			setNombre("Pedro")
			setApellido("Manzana")
			setUserName("manzanita85")
			setPassword("applepedro")
			setDireccionMail("thePedro@gmail.com")
			setFechaNacimiento(new Date())
		]
		usr3.setCodigoValidacion(CodeGenerator.newCode(usr3))
		
		usr4 = new Usuario =>[
			setNombre("Federico")
			setApellido("Mancuello")
			setUserName("mancuDios")
			setPassword("123456")
			setDireccionMail("fedeCai@gmail.com")
			setFechaNacimiento(new Date())
		]
		usr4.setCodigoValidacion(CodeGenerator.newCode(usr4))
		
		try {
			conn = this.getConnection()
			var ScriptRunner runner = new ScriptRunner(conn, false, false)
            runner.runScript(new BufferedReader(new FileReader("crearTableUsuarios.sql")));
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}
		em = mock(EnviadorDeMails)
		dao = new UsuarioDaoSql()
		servicio = new ServicioUsuarioSql(dao, em)
	}

	@After def void tearDown() throws Exception {
		try {
			conn = this.getConnection()
			var ScriptRunner runner = new ScriptRunner(conn, false, false)
            runner.runScript(new BufferedReader(new FileReader("borrarTablaUsuarios.sql")));
		
		} catch (SQLException e) {
			e.printStackTrace()
		} finally {
			if(ps !== null) ps.close()
			if(conn !== null) conn.close()
		}
	}

	@Test def void testRegistrarUsuario() throws EnviarMailException {
		servicio.registrarUsuario(usr1)
		verify(em, times(1)).enviarMail(any() as Mail);
		var Usuario newUsr = dao.getUsuarioPorUsername(usr1.getUserName())
		assertEquals(usr1.getUserName(), newUsr.getUserName())
	}

	@Test(expected=UsuarioYaExisteException) def void testRegistrarUsuarioConExcepcion() {
		servicio.registrarUsuario(usr1)
		servicio.registrarUsuario(usr1)
	}

	@Test def void testValidarUsuario() {
		dao.insertUsuario(usr2)
		var String codigo = CodeGenerator.newCode(usr2)
		servicio.validarCuenta(codigo)
		var Usuario usrSql = dao.getUsuarioPorUsername("mrRobot")
		var Boolean expected = true
		var Boolean actual = usrSql.getValidated()
		assertEquals(expected, actual)
	}

	@Test(expected=ValidacionException) def void testValidarUsuarioConExcepcion() {
		var String codigo = CodeGenerator.newCode(usr3)
		servicio.validarCuenta(codigo)
	}

	@Test def void testLoguearUsuario() {
		servicio.registrarUsuario(usr2)
		servicio.loggearUsuario("mrRobot", "asge_57.-egtq")
		var Usuario usrSql = dao.getUsuarioPorUsername("mrRobot")
		assertEquals(usr2.getUserName(), usrSql.getUserName())
		assertEquals(usr2.getPassword(), usrSql.getPassword())
	}

	@Test(expected=UsuarioNoExiste) def void testLoguearUsuarioConExcepcionAlNoEstarRegistrado() {
		servicio.loggearUsuario("rodrigo94", "asdf")
	}

	@Test(expected=UsuarioNoExiste) def void testLoguearUsuarioConExcepcionAlNoSerLaPasswordCorrecta() {
		var Usuario usr = new Usuario()
		usr.setNombre("Armando")
		usr.setApellido("Manzanares")
		usr.setUserName("manzanota")
		usr.setPassword("quieroMiAuto")
		usr.setDireccionMail("aManzanares@gmail.com")
		usr.setFechaNacimiento(new Date())
		usr.setCodigoValidacion(CodeGenerator.newCode(usr))
		servicio.registrarUsuario(usr)
		servicio.loggearUsuario("manzanota", "armando123")
	}

	@Test def void testCambiarPassword() {
		dao.insertUsuario(usr2)
		servicio.cambiarPassword(usr2.getUserName(), usr2.getPassword(), "nuevaPassword")
		var Usuario usrSql = dao.getUsuarioPorUsername(usr2.getUserName())
		assertEquals("nuevaPassword", usrSql.getPassword())
	}

	@Test(expected=NuevaPasswordInválida) def void testCambiarPasswordConExcepcion() {
		servicio.registrarUsuario(usr4)
		servicio.cambiarPassword("mancuDios", "123456", "123456")
	}

	def private Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver")
		return DriverManager.getConnection("jdbc:mysql://localhost/?user=root&password=root")
	}

}
