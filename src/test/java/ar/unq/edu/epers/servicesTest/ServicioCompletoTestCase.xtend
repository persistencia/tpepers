package ar.unq.edu.epers.servicesTest

import ar.unq.edu.epers.model.Auto
import ar.unq.edu.epers.model.IUsuario
import ar.unq.edu.epers.model.Ubicacion
import ar.unq.edu.epers.model.Usuario
import ar.unq.edu.epers.services.ServicioCompleto
import java.util.Date
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.*
import ar.unq.edu.epers.model.Familiar
import ar.unq.edu.epers.model.Empresa
import ar.unq.edu.epers.hbmUtils.SessionManager
import ar.unq.edu.epers.model.ReservaException

class ServicioCompletoTestCase {
	Auto auto
	IUsuario user
	ServicioCompleto serv
	Ubicacion quilmes
	
	@Before
	def  void setUp(){
		quilmes = new Ubicacion("Quilmes")
		auto = new Auto("Chevrolet", "Camaro", 2004, "CSI782", new Familiar, 7400000.0, quilmes)
		serv = new ServicioCompleto
		user = new Usuario =>[
			setNombre("Federico")
			setApellido("Mancuello")
			setUserName("fedeRoj0")
			setPassword("rojodemivida")
			setDireccionMail("fedem11@gmail.com")
			setFechaNacimiento(new Date())
			setReservas = newArrayList
		]
	}
	
	@Test def void testHacerReserva(){
		val destino = new Ubicacion("La Plata")
		val inicio = new Date(564594185) //Agregue un inicio porque estaba vacio
		val fin = new Date(564564185)
		serv.hacerReserva(123, quilmes, destino, inicio, fin, auto, user)
		var expected = 1
		var autoActual = auto.reservas.size
		var userActual = user.reservas.size
		assertEquals(expected, autoActual)
		assertEquals(expected, userActual)
	}
	
	@Test def void testHacerReservaEmpresarial(){
		var empresa = new Empresa => [
			cantidadMaximaDeReservasActivas = 5
			valorMaximoPorDia = 157528.0
			usuarios.add(user)
			]
		serv.hacerReservaEmpresrial (854, quilmes, new Ubicacion("La Plata"), new Date(96561212), new Date(965612135), auto, user, empresa, "Pedro Castro", "CEO")
		var expected = 1
		var autoActual = auto.reservas.size
		var empresaActual = empresa.reservas.size
		assertEquals(expected, autoActual) 
		assertEquals(expected, empresaActual)
	}
	
	@Test(expected = ReservaException) 
	def void testHacerReservaEmpresaSinReservas(){
		var empresa = new Empresa => [
			cantidadMaximaDeReservasActivas = 0
			valorMaximoPorDia = 0.0
			usuarios.add(user)
			]
		serv.hacerReservaEmpresrial (854, quilmes, new Ubicacion("La Plata"), new Date(96561212), new Date(965612135), auto, user, empresa, "Pedro Castro", "CEO")
	}
}